<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Страница - ОШИБКА</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/main.css">
<script src="scripts/jquery-3.2.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1>
				Ой...что-то пошло не так. Чтобы вернуться на Главную страницу
				нажмите <a href="index.jsp">ЗДЕСЬ</a>
			</h1>
		</div>
	</div>
</body>
</html>