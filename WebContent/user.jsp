<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/user.css">
<script src="scripts/jquery-3.2.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<title>Кабинет Пользователя</title>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>

			<div class="logout-button col-lg-2 col-sm-2 col-xs-2">
				<form action="Controller?action=logout" method="POST">
					<button type="submit" class="btn btn-info">Выйти из
						системы</button>
				</form>
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-5 col-sm-4 col-xs-12">
				<span class="label label-success"><c:out
						value="${userPage.getMessage()}"></c:out></span>
				${userPage.setMessage(null)}
			</div>
		</div>
		<c:set var="userViewModel" value="${userPage.getUserViewModel()}">
		</c:set>
		<div class="row user-editor">
			<div class="col-lg-12 col-sm-12 col-xs-12">
				<h2>
					<b>Личные данные пользователя</b>
				</h2>
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Логин</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getLogin()}" />
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Email</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getEmail()}" />
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Имя</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getName()}" />
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Фамилия</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getSurname()}" />
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Адрес</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getAddress()}" />
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Стаж</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getYearsOfExperience()}" />
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-2 col-sm-2 col-xs-6">
				<b>Должность</b>
			</div>
			<div class="col-lg-10 col-sm-10 col-xs-6">
				<c:out value="${userViewModel.getPosition()}" />
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-12 col-sm-12 col-xs-12">
				<form action="Controller?action=openEditor" method="POST">
					<input type="hidden" name="id" value="${userViewModel.getId()}">
					<button type="submit" class="btn btn-warning change-user-button">Изменить</button>
				</form>
			</div>
		</div>
	</div>

</body>
</html>