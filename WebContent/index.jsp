<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Главная</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/index.css">
<script src="scripts/jquery-3.2.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/index.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-6 col-sm-6 col-xs-6">
				<div class="spacer">
					<span class="err" id="err"><c:out
							value="${indexPage.getMessage()}" /></span>
					${indexPage.setMessage(null)}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-7">
				<h2>
					Здравствуйте!<br /> Пожалуйста, войдите в систему:
				</h2>
				<div class="col-lg-12 col-sm-12 col-xs-12"></div>

				<form class="form-horizontal" action="Controller?action=authorise"
					method="POST">

					<div class="form-group">
						<label class="control-label col-sm-2" for="login">Имя
							пользователя:</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="login" name="login"
								value="${indexPage.getAuthorisedUserView().getLogin()}"
								maxlength="40">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="password">Пароль:</label>
						<div class="col-sm-3">
							<input type="password" class="form-control" id="password"
								name="password"
								value="${indexPage.getAuthorisedUserView().getPassword()}"
								maxlength="40"> ${indexPage.setAuthorisedUserView(null)}
						</div>
					</div>


					<div class="col-sm-10 sign-in-button">
						<button type="submit" class="btn btn-default" id="singInButton">Войти</button>
					</div>


				</form>

			</div>


		</div>
	</div>
</body>
</html>