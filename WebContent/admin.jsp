<%@page import="model.UserSortField"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/admin.css">
<script src="scripts/jquery-3.2.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/admin.js"></script>
<title>Администрирование</title>
</head>
<body>

	<c:set var="sortField"
		value="${adminPage.getCriteria().getSortField()}">
	</c:set>

	<c:set var="ascending" value="${adminPage.getCriteria().isAscending()}">
	</c:set>

	<c:choose>
		<c:when test="${ascending}">
			<c:set var="arrow" value="images/up.png">
			</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="arrow" value="images/down.png">
			</c:set>
		</c:otherwise>
	</c:choose>

	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="add-user-button col-lg-3 col-sm-6 col-xs-12">
				<form action="Controller?action=openEditor" method="POST">
					<button type="submit" class="btn btn-success">Добавить
						Пользователя</button>
				</form>
			</div>
			<div class="col-lg-7 col-sm-5 col-xs-12 admin-messages">
				<span class="label label-success"><c:out
						value="${adminPage.getMessage()}"></c:out></span>${adminPage.setMessage(null)}</div>

			<!-- Desktop version of the Logout Button -->
			<div class="logout-button-lg col-lg-2">
				<form action="Controller?action=logout" method="POST">
					<button type="submit" class="btn btn-info">Выйти из
						системы</button>
				</form>
			</div>
			<!--  -->

			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
		</div>

		<!-- Desktop version of the table's header  -->
		<div class="row table-header-row">
			<div class="col-lg-10 col-sm-10 header-padding">
				<div class="col-lg-2 col-sm-2">
					<a
						href="Controller?action=getUsers&field=login&ascending=${!ascending}"><b>Логин</b></a>
					<c:if test="${sortField == 'LOGIN'}">
						<img src="${arrow}">
					</c:if>
				</div>
				<div class="col-lg-2 col-sm-2">
					<a
						href="Controller?action=getUsers&field=email&ascending=${!ascending}"><b>E-mail</b></a>
					<c:if test="${sortField == 'EMAIL'}">
						<img src="${arrow}">
					</c:if>
				</div>
				<div class="col-lg-2 col-sm-2">
					<a
						href="Controller?action=getUsers&field=name&ascending=${!ascending}"><b>Имя</b></a>
					<c:if test="${sortField == 'NAME'}">
						<img src="${arrow}">
					</c:if>
				</div>
				<div class="col-lg-2 col-sm-2">
					<a
						href="Controller?action=getUsers&field=surname&ascending=${!ascending}"><b>Фамилия</b></a>
					<c:if test="${sortField == 'SURNAME'}">
						<img src="${arrow}">
					</c:if>
				</div>
				<div class="col-lg-1 col-sm-1">
					<a
						href="Controller?action=getUsers&field=years&ascending=${!ascending}"><b>Стаж</b></a>
					<c:if test="${sortField == 'YEARS'}">
						<img src="${arrow}">
					</c:if>
				</div>
				<div class="col-lg-2 col-sm-2">
					<a
						href="Controller?action=getUsers&field=address&ascending=${!ascending}"><b>Адрес</b></a>
					<c:if test="${sortField == 'ADDRESS'}">
						<img src="${arrow}">
					</c:if>
				</div>
				<div class="col-lg-2 col-sm-2">
					<a
						href="Controller?action=getUsers&field=position&ascending=${!ascending}"><b>Должность</b></a>
					<c:if test="${sortField == 'POSITION'}">
						<img src="${arrow}">
					</c:if>
				</div>
			</div>
			<div class="col-lg-2 col-sm-2"></div>
		</div>
		<!--  -->

		<!-- Mobile version of table's header and results  -->
		<div class="row table-results-row-xs">
			<c:forEach items="${adminPage.getUserList()}" var="user"
				varStatus="loop">
				<c:choose>
					<c:when test="${loop.index % 2 != 0}">
						<c:set var="stripped" value="stripped">
						</c:set>
					</c:when>
					<c:otherwise>
						<c:set var="stripped" value="">
						</c:set>
					</c:otherwise>
				</c:choose>
				<div class="${stripped}">
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=login&ascending=${!ascending}"><b>Логин</b></a>
						<c:if test="${sortField == 'LOGIN'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getLogin()}" />
					</div>
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=email&ascending=${!ascending}"><b>E-mail</b></a>
						<c:if test="${sortField == 'EMAIL'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getEmail()}" />
					</div>
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=name&ascending=${!ascending}"><b>Имя</b></a>
						<c:if test="${sortField == 'NAME'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getName()}" />
					</div>
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=surname&ascending=${!ascending}"><b>Фамилия</b></a>
						<c:if test="${sortField == 'SURNAME'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getSurname()}" />
					</div>
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=years&ascending=${!ascending}"><b>Стаж</b></a>
						<c:if test="${sortField == 'YEARS'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getYearsOfExperience()}" />
					</div>
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=address&ascending=${!ascending}"><b>Адрес</b></a>
						<c:if test="${sortField == 'ADDRESS'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getAddress()}" />
					</div>
					<div class="col-xs-6">
						<a
							href="Controller?action=getUsers&field=position&ascending=${!ascending}"><b>Должность</b></a>
						<c:if test="${sortField == 'POSITION'}">
							<img src="${arrow}">
						</c:if>
					</div>
					<div class="col-xs-6">
						<c:out value="${user.getPosition()}" />
					</div>
					<div class="col-xs-12">
						<div class="open-editor-button col-xs-6">
							<form action="Controller?action=openEditor" method="POST">
								<input type="hidden" name="id" value="${user.getId()}">
								<button type="submit" class="btn btn-warning">Изменить</button>
							</form>
						</div>
						<div class="delete-user-button col-xs-6">
							<form action="Controller?action=deleteUser" method="POST">
								<input type="hidden" name="id" value="${user.getId()}">
								<button type="button" class="btn btn-danger deleteButtonXs">Удалить</button>
							</form>
						</div>
					</div>
				</div>
				<hr />
			</c:forEach>
		</div>
		<!--  -->

		<!-- Desktop version of table results  -->
		<c:forEach items="${adminPage.getUserList()}" var="user"
			varStatus="loop">
			<c:choose>
				<c:when test="${loop.index % 2 != 0}">
					<c:set var="stripped" value="stripped">
					</c:set>
				</c:when>
				<c:otherwise>
					<c:set var="stripped" value="">
					</c:set>
				</c:otherwise>
			</c:choose>

			<div class="row table-results-row" style="visibility: hidden">
				<div class="col-lg-10 col-sm-10 rows-padding ${stripped}">
					<div class="col-lg-2 col-sm-2">
						<c:out value="${user.getLogin()}" />
					</div>
					<div class="col-lg-2 col-sm-2">
						<c:out value="${user.getEmail()}" />
					</div>
					<div class="col-lg-2 col-sm-2">
						<c:out value="${user.getName()}" />
					</div>
					<div class="col-lg-2 col-sm-2">
						<c:out value="${user.getSurname()}" />
					</div>
					<div class="col-lg-1 col-sm-1">
						<c:out value="${user.getYearsOfExperience()}" />
					</div>
					<div class="col-lg-2 col-sm-2">
						<c:out value="${user.getAddress()}" />
					</div>
					<div class="col-lg-2 col-sm-2">
						<c:out value="${user.getPosition()}" />
					</div>
				</div>
				<div class="col-lg-2 col-sm-2">
					<div class="open-editor-button col-lg-1 col-sm-1">
						<form action="Controller?action=openEditor" method="POST">
							<input type="hidden" name="id" value="${user.getId()}">
							<button type="submit" class="btn btn-warning editButton">Изменить</button>
						</form>
					</div>

					<div class="delete-user-button col-lg-1 col-sm-1">
						<form action="Controller?action=deleteUser" method="POST">
							<input type="hidden" name="id" value="${user.getId()}">
							<button type="button" class="btn btn-danger deleteButton">Удалить</button>
						</form>
					</div>

				</div>
			</div>
		</c:forEach>
		<!--  -->

		<!-- Mobile version Logout Button  -->
		<div class="row logout-button-xs">
			<div class="col-xs-12">
				<div class="col-xs-12"></div>
				<form action="Controller?action=logout" method="POST">
					<button type="submit" class="btn btn-info">Выйти из
						системы</button>
				</form>
			</div>
		</div>
		<!--  -->

	</div>

	<!-- Modal dialog asking about deleting a user -->
	<div class="modal fade" id="deleteModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Удаление Пользователя</h4>
				</div>
				<div class="modal-body">
					<p>Вы уверены, что хотите удалить этого Пользователя?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" id="yesButton">Да</button>
					<button type="button" class="btn btn-info" data-dismiss="modal">Нет</button>
				</div>
			</div>
		</div>
	</div>
	<!--  -->

</body>
</html>