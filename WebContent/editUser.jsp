<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="styles/bootstrap.min.css">
<link rel="stylesheet" href="styles/main.css">
<link rel="stylesheet" href="styles/editUser.css">
<script src="scripts/jquery-3.2.1.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/editUser.js"></script>
<title>Изменить данные</title>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-4 col-sm-4 col-xs-12"></div>
			<div class="col-lg-8 col-sm-8 col-xs-12 edit-user-messages"
				id="edit-user-messages">
				<c:forEach items="${editUserPage.getInputErrors()}" var="error">
					<div>
						<c:out value="${error}" />
					</div>
				</c:forEach>
				${editUserPage.setInputErrors(null)}
			</div>
		</div>
		<c:set var="userViewModel" value="${editUserPage.getUserViewModel()}">
		</c:set>

		<div class="row col-xs-12 edit-user-inputs-xs">
			<form action="Controller?action=saveUser" method="POST">
				<input type="hidden" name="adminPage"
					value="${sessionScope.adminPage}"> <input type="hidden"
					name="id" value="${userViewModel.getId()}">
				<div class="col-xs-12">
					<div class="form-group">
						<span><b>Логин</b></span><input type="text" name="login"
							class="form-control"
							title="Только латинские буквы и арабские цифры"
							value="${userViewModel.getLogin()}" maxlength="30">
					</div>
					<input type="hidden" name="password">
					<c:if
						test="${adminPage != null && userViewModel.getId() == null || userViewModel.getId() == 0 }">

						<div class="form-group">
							<span><b>Пароль</b></span><input type="password"
								name="newPassword" class="form-control" maxlength="40">
						</div>
						<div class="form-group">
							<span><b>Повторить пароль</b></span><input type="password"
								name="ReNewPassword" class="form-control" maxlength="40">
						</div>


					</c:if>
					<div class="form-group">
						<span><b>E-mail</b></span><input type="text" name="email"
							class="form-control" value="${userViewModel.getEmail()}"
							maxlength="40">
					</div>
					<div class="form-group">
						<span><b>Имя</b></span><input type="text" name="name"
							class="form-control" value="${userViewModel.getName()}"
							maxlength="40">
					</div>
					<div class="form-group">
						<span><b>Фамилия</b></span><input type="text" name="surname"
							class="form-control" value="${userViewModel.getSurname()}"
							maxlength="40">
					</div>
					<div class="form-group">
						<span><b>Адрес</b></span><input type="text" name="address"
							class="form-control" value="${userViewModel.getAddress()}"
							maxlength="40">
					</div>
					<c:if test="${adminPage != null}">
						<div class="form-group">
							<span><b>Стаж</b></span><input type="text" name="years"
								class="form-control"
								title="Целое число лет от 0 до 30 включительно"
								value="${userViewModel.getYearsOfExperience()}" maxlength="2">
						</div>
					</c:if>
					<c:if test="${adminPage != null}">
						<div class="form-group">
							<span><b>Должность</b></span><input type="text" name="position"
								class="form-control" value="${userViewModel.getPosition()}"
								maxlength="30">
						</div>
					</c:if>
				</div>
				<c:remove var="userViewModel" scope="session" />
				<div class="col-xs-12"></div>

				<div class="col-xs-12 editor-actions">
					<div class="btn-group-md">
						<button type="submit" class="btn btn-primary saveUserButton">Сохранить</button>
						<button type="button" class="btn btn-warning cancelButton">Отмена</button>
						<c:if test="${adminPage == null}">
							<button type="button" class="btn btn-info changePassword">Изменить
								пароль</button>
						</c:if>
					</div>
				</div>
			</form>
		</div>

		<div class="row col-lg-12 col-sm-12 edit-user-inputs-lg">
			<form action="Controller?action=saveUser" method="POST">
				<input type="hidden" name="adminPage" value="${adminPage}">
				<input type="hidden" name="id" value="${userViewModel.getId()}">
				<div class="input-group">
					<span class="input-group-addon"><b>Логин</b></span><input
						type="text" name="login" class="form-control"
						title="Только латинские буквы и арабские цифры"
						value="${userViewModel.getLogin()}" maxlength="30">
				</div>
				<input type="hidden" name="password">
				<c:if
					test="${adminPage != null && userViewModel.getId() == null || userViewModel.getId() == 0}">
					<div class="input-group">
						<span class="input-group-addon"><b>Пароль</b></span><input
							type="password" name="newPassword" class="form-control"
							maxlength="40">
					</div>

					<div class="input-group">
						<span class="input-group-addon"><b>Повторить пароль</b></span><input
							type="password" name="ReNewPassword" class="form-control"
							maxlength="40">
					</div>

				</c:if>
				<div class="input-group">
					<span class="input-group-addon"><b>E-mail</b></span><input
						type="text" name="email" class="form-control"
						value="${userViewModel.getEmail()}" maxlength="40">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><b>Имя</b></span><input type="text"
						name="name" class="form-control"
						value="${userViewModel.getName()}" maxlength="40">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><b>Фамилия</b></span><input
						type="text" name="surname" class="form-control"
						value="${userViewModel.getSurname()}" maxlength="40">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><b>Адрес</b></span><input
						type="text" name="address" class="form-control"
						value="${userViewModel.getAddress()}" maxlength="40">
				</div>
				<c:if test="${adminPage != null}">
					<div class="input-group">
						<span class="input-group-addon"><b>Стаж</b></span> <input
							type="text" name="years" class="form-control"
							title="Целое число лет от 0 до 30 включительно"
							value="${userViewModel.getYearsOfExperience()}" maxlength="2">
					</div>
				</c:if>
				<c:if test="${adminPage != null}">
					<div class="input-group">
						<span class="input-group-addon"><b>Должность</b></span> <input
							type="text" name="position" class="form-control"
							value="${userViewModel.getPosition()}" maxlength="30">
					</div>
				</c:if>
				<c:remove var="userViewModel" scope="session" />
				<div class="col-lg-12 col-sm-12"></div>

				<div class="col-lg-12 col-sm-12 editor-actions">
					<div class="btn-group-md">
						<button type="submit" class="btn btn-primary saveUserButton">Сохранить</button>
						<button type="button" class="btn btn-warning cancelButton">Отмена</button>
						<c:if test="${adminPage == null}">
							<button type="button" class="btn btn-info changePassword">Изменить
								пароль</button>
						</c:if>
					</div>
				</div>
			</form>
		</div>

	</div>

	<div class="modal fade" id="changePasswordModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Измените пароль</h4>
				</div>
				<div class="modal-body">
					<div id="edit-password-messages"></div>
					<p>Пароль может содержать только латинские буквы, арабские
						цифры. Должен быть не короче 6 символов, иметь, как минимум, одну
						большую букву, одну маленькую букву и одну цифру</p>
					<div class="change-pas-modal-xs">
						<div class="form-group">
							<span><b>Текущий пароль</b></span><input type="password"
								name="oldPas" class="form-control" maxlength="40">
						</div>
						<div class="form-group">
							<span><b>Новый пароль</b></span><input type="password"
								name="newPas" class="form-control" maxlength="40">
						</div>
						<div class="form-group">
							<span><b>Повторить новый пароль</b></span><input type="password"
								name="reNewPas" class="form-control" maxlength="40">
						</div>
					</div>
					<div class="change-pas-modal-lg">
						<div class="input-group">
							<span class="input-group-addon"><b>Текущий пароль</b></span><input
								type="password" name="oldPas" class="form-control"
								maxlength="40">
						</div>
						<div class="input-group">
							<span class="input-group-addon"><b>Новый пароль</b></span><input
								type="password" name="newPas" class="form-control"
								maxlength="40">
						</div>
						<div class="input-group">
							<span class="input-group-addon"><b>Повторить новый
									пароль</b></span><input type="password" name="reNewPas"
								class="form-control" maxlength="40">
						</div>
					</div>
					<button type="button" class="btn btn-danger savePassword">Сохранить</button>
					<button type="button" class="btn btn-info" data-dismiss="modal">Отменить</button>
				</div>
			</div>
		</div>
	</div>

</body>
</html>