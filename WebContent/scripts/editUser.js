$(document).ready(function() {
	
	$(".saveUserButton").click(editUserValidate);
	$(".cancelButton").click(cancelChanges);
	$("button.changePassword").click(function(){
		
		$("#edit-password-messages").empty();
		$("#changePasswordModal").modal("show");
	});
	
	$(".savePassword").click(savePassword);
});

function editUserValidate() {

	var result = true;

	var LOGIN_REG_EXP = /^[0-9a-zA-Z]\S*$/, 
		YEARS_REG_EXP = /^\d+$/,
		EMAIL_REG_EXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 
		NOT_VALID_LOGIN = "Логин может начинаться с латинской буквы или цифры, без пробелов", 
		NOT_VALID_EMAIL = "E-mail неправильного формата.",
		NOT_VALID_YEARS = "В поле 'Стаж' можно ввести только целое число от 0 до 30.",
		FILL_FIELD = "Все поля обязательны для заполнения.",
		PASSWORD_REG_EXP = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/, 
		NOT_VALID_PAS = "Пароль может содержать только латинские буквы, арабские цифры. Должен быть не короче 6 символов, иметь, как минимум, одну большую букву, одну маленькую букву и одну цифру.",
		PASS_NOT_EQUAL = "Пароли не совпадают";

	var errAll = $("#edit-user-messages");

	errAll.empty();

	var parentForm = $(this).parents("form");

	var log = parentForm.find("input[name='login']").val().trim(),
		email = parentForm.find("input[name='email']").val().trim(),
		name = parentForm.find("input[name='name']").val().trim(),
		sur = parentForm.find("input[name='surname']").val().trim(), 
		addr = parentForm.find("input[name='address']").val().trim(),
		pos = parentForm.find("input[name='position']").val(),
		years = parentForm.find("input[name='years']").val();
	
	var admin = $("input[name='adminPage']").val().trim();
	var userId = $("input[name='id']").val().trim();
	
	if (admin != "") {
		
		var newPassword = parentForm.find("input[name='newPassword']").val();
		
		var ReNewPassword = parentForm.find("input[name='ReNewPassword']").val();
		
		if (newPassword != null) {
			
			newPassword.trim();
		}
		
		if (ReNewPassword != null) {
			
			ReNewPassword.trim();
		}
		
		if (pos != null) {
			
			pos.trim();
		}
		
		if (years != null) {
			
			years.trim();
		}
		
		if (!log || !email || !name || !sur || !addr || !pos || !years || (!newPassword || !ReNewPassword) && (userId == "" || userId == 0) ) {
			
			errAll.append("<div>" + FILL_FIELD + "</div>");
			result = false;
		}
		
		if (newPassword && ReNewPassword && newPassword !== ReNewPassword) {
			errAll.append("<div>" + PASS_NOT_EQUAL + "</div>");
			result = false;
		}
		
		if ((userId == "" || userId == 0) && newPassword && !(PASSWORD_REG_EXP.test(newPassword))) {
			errAll.append("<div>" + NOT_VALID_PAS + "</div>");
			result = false;
		}
		
		if (years && (years < 0 || years > 30 || !(YEARS_REG_EXP.test(years)))) {
			
			errAll.append("<div>" + NOT_VALID_YEARS + "</div>");
			result = false;
		} 
		
	} else {
		
		if (!log || !email || !name || !sur || !addr) {
			
			errAll.append("<div>" + FILL_FIELD + "</div>");
			result = false;
		}
	}

	if (log && !(LOGIN_REG_EXP.test(log))) {
		errAll.append("<div>" + NOT_VALID_LOGIN + "</div>");
		result = false;
	}

	if (email && !(EMAIL_REG_EXP.test(email))) {
		errAll.append("<div>" + NOT_VALID_EMAIL + "</div>");
		result = false;
	}
	
	if (errAll.children.length > 0) {
		
		var elmnt = document.getElementById("edit-user-messages");
		elmnt.scrollIntoView();
	}
	
	return result;	
}

function savePassword() {

	if (validateNewPasswordModal()) {
		
		var div = $(".savePassword").prevAll().filter(function(){
			return	$(this).css("display") != "none"; 
		});
		
		var login = $("input[name='login']").val().trim(),
		 	oldPassword =  div.find("input[name='oldPas']").val().trim(),
		 	newPassword = div.find("input[name='newPas']").val().trim(),
		 	errAll = $("#edit-user-messages"),
		 	errModal = $("#edit-password-messages"),
		 	SUCCESS = "Пароль был успешно изменён.",
			ERROR = "Текущий пароль неверный.";
		
		errAll.empty();
	
		$.ajax({
			method : "POST",
			data : {
				login: login,
				oldPassword : oldPassword,
				newPassword : newPassword
			},
			url : "Controller?action=updatePass",
			success : function() {
				
				errAll.css("color", "green");
				errAll.append("<div>" + SUCCESS + "</div>");
				$("#changePasswordModal").modal("hide");
			},
			error: function() {
				
				errModal.append("<div>" + ERROR + "</div>");	
			}
		});
	}
}

function validateNewPasswordModal() {

	var result = true;

	var PASS_NOT_EQUAL = "Новые пароли не совпадают.", 
		PASSWORD_REG_EXP = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/,
		NOT_VALID_PAS_NEW = "Новый пароль неверного формата.",
		FILL_FIELD = "Все поля обязательны для заполнения.";

	var errAll = $("#edit-password-messages");

	errAll.empty();
	
	var div = $(".savePassword").prevAll().filter(function(){
		return	$(this).css("display") != "none"; 
	});

	var oldPas = div.find("input[name='oldPas']").val().trim(),
		newPas = div.find("input[name='newPas']").val().trim(),
		reNewPas = div.find("input[name='reNewPas']").val().trim();
		
	if ( !oldPas || !newPas || !reNewPas) {
		errAll.append("<div>" + FILL_FIELD + "</div>");
		result = false;
	}
	
	if (newPas && !(PASSWORD_REG_EXP.test(newPas))) {
		errAll.append("<div>" + NOT_VALID_PAS_NEW + "</div>");
		result = false;
	}
	
	if (newPas && reNewPas && newPas !== reNewPas) {
		errAll.append("<div>" + PASS_NOT_EQUAL + "</div>");
		result = false;
	}
	
	return result;	
}


function cancelChanges() {
	
	var parentForm = $(this).parents("form");
	var admin = $("input[name='adminPage']").val().trim();

	if (admin != "") {

		window.location.replace("admin.jsp");

	} else {

		window.location.replace("user.jsp");
	}
}