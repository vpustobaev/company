function logValidate() {

	var result = true;

	var FILL_FIELD = "Все поля должны быть заполнены";

	var err = $("#err"),
		log = $("#login").val(), 
		pass = $("#password").val();

	err.html("");

	if (!log || !pass) {
		err.text(FILL_FIELD);
		result = false;
	}

	return result;
}

$(document).ready(function() {
	$("#singInButton").click(logValidate);
});