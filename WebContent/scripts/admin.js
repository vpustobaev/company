$(document).ready(function() {

	adjustResultsRowHeight();
	$(".deleteButton").click(deleteUser);
	$(".deleteButtonXs").click(deleteUser);

});

function adjustResultsRowHeight() {

	var results = $(".rows-padding");

	var maximumHeight = results[0].clientHeight;

	for (var i = 1; i < results.length; i++) {

		if (maximumHeight < results[i].clientHeight) {

			maximumHeight = results[i].clientHeight;
		}
	}

	results.css("height", maximumHeight);
	$(".editButton").css("margin", "+=2px");
	$(".editButton").css("height", maximumHeight);
	$(".deleteButton").css("margin", "+=2px");
	$(".deleteButton").css("height", maximumHeight);
	$(".table-results-row").css("visibility", "visible");
}


function deleteUser() {

		var form = $(this).parent();
		
		$("#deleteModal").modal("show");
		
		$("#yesButton").click(function() {
			
			$("#deleteModal").modal("hide");
			
			form.submit();
		});
}

