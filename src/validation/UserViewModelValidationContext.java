package validation;

import java.util.ArrayList;

import model.UserViewModel;

public class UserViewModelValidationContext {

	private String emailExistsError;

	private String loginExistsError;

	private String yearsOfExperienceError;

	private ArrayList<String> inputErrors;

	private UserViewModel model;

	public UserViewModelValidationContext(UserViewModel model) {

		this.model = model;

		this.inputErrors = new ArrayList<String>();

		this.emailExistsError = "������������ � Email '" + model.getEmail() + "' ��� ����������.";

		this.loginExistsError = "������������ � ������� '" + model.getLogin() + "' ��� ����������.";

		this.yearsOfExperienceError = "� ���� '����' ����� ������ ������ ����� ����� �� 0 �� 30";

	}

	public String getEmailExistsError() {
		return emailExistsError;
	}

	public String getLoginExistsError() {
		return loginExistsError;
	}

	public UserViewModel getModel() {
		return model;
	}

	public ArrayList<String> getInputErrors() {
		return inputErrors;
	}

	public String getYearsOfExperienceError() {
		return yearsOfExperienceError;
	}

}
