package validation;

import java.sql.SQLException;

import dao.UserDAO;

public class UserViewModelValidator {

	private UserDAO dao;

	public UserViewModelValidator(UserDAO dao) {

		this.dao = dao;
	}

	public void validate(UserViewModelValidationContext context) throws SQLException {

		Integer userId = context.getModel().getId();
		String userLogin = context.getModel().getLogin();
		String userEmail = context.getModel().getEmail();

		if (!this.dao.isUserUniqueByEmail(userEmail, userId)) {

			context.getInputErrors().add(context.getEmailExistsError());
		}

		if (!this.dao.isUserUniqueByLogin(userLogin, userId)) {

			context.getInputErrors().add(context.getLoginExistsError());
		}

		Integer yearsOfExperience = context.getModel().getYearsOfExperience();

		if (yearsOfExperience == null || yearsOfExperience > 30) {

			context.getInputErrors().add(context.getYearsOfExperienceError());
		}
	}

}
