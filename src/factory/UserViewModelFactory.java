package factory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import constant.PageAttributes;
import entity.User;
import model.AdminPageViewModel;
import model.UserPageViewModel;
import model.UserViewModel;
import util.ParsingUtilities;

public class UserViewModelFactory {

	public UserViewModel getUserViewModelFromRequestParameters(HttpServletRequest request) {

		HttpSession session = request.getSession();

		UserViewModel model = new UserViewModel();

		String password = request.getParameter("password").trim();
		String newPassword = request.getParameter("newPassword");

		String position = request.getParameter("position");
		String yearsOfExperience = request.getParameter("years");

		AdminPageViewModel adminPage = (AdminPageViewModel) session.getAttribute(PageAttributes.ADMIN_PAGE);

		if (adminPage != null) {

			if (newPassword != null) {

				password = newPassword.trim();
			}

			model.setPosition(position.trim());
			model.setYearsOfExperience(ParsingUtilities.tryParseInt(yearsOfExperience.trim()));

		} else {

			UserPageViewModel userPage = (UserPageViewModel) session.getAttribute(PageAttributes.USER_PAGE);

			model.setPosition(userPage.getUserViewModel().getPosition());
			model.setYearsOfExperience(userPage.getUserViewModel().getYearsOfExperience());
		}

		model.setPassword(password);

		String id = request.getParameter("id").trim();

		if (id != "") {

			model.setId(ParsingUtilities.tryParseInt(id));
		}

		else {

			model.setId(0);
		}

		String login = request.getParameter("login").trim();
		String name = request.getParameter("name").trim();
		String surname = request.getParameter("surname").trim();
		String email = request.getParameter("email").trim();
		String address = request.getParameter("address").trim();

		model.setLogin(login);
		model.setName(name);
		model.setSurname(surname);
		model.setEmail(email);
		model.setAddress(address);

		return model;
	}

	public UserViewModel getUserViewModelFromUser(User user) {

		UserViewModel userView = new UserViewModel();

		if (user != null) {

			userView.setId(user.getId());
			userView.setLogin(user.getLogin());
			userView.setPassword(user.getPassword());
			userView.setEmail(user.getEmail());
			userView.setName(user.getName());
			userView.setSurname(user.getSurname());
			userView.setAddress(user.getAddress());
			userView.setYearsOfExperience(user.getYearsOfExperience());
			userView.setPosition(user.getPosition());
			userView.setRole(user.getRole());
		}

		return userView;
	}

}
