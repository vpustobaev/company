package factory;

import javax.servlet.http.HttpServletRequest;

import model.UserSearchCriteria;
import model.UserSortField;

public class UserSearchCriteriaFactory {

	public UserSearchCriteria getUserSearchCriteria(HttpServletRequest request) {

		String field = request.getParameter("field");
		boolean ascending = Boolean.valueOf(request.getParameter("ascending"));

		UserSearchCriteria criteria = new UserSearchCriteria();
		UserSortField sortField = null;

		if (field != null) {

			switch (field) {

			case "login":

				sortField = UserSortField.LOGIN;
				break;

			case "email":

				sortField = UserSortField.EMAIL;
				break;

			case "name":

				sortField = UserSortField.NAME;
				break;

			case "surname":

				sortField = UserSortField.SURNAME;
				break;

			case "address":

				sortField = UserSortField.ADDRESS;
				break;

			case "years":

				sortField = UserSortField.YEARS;
				break;

			case "position":

				sortField = UserSortField.POSITION;
				break;

			}
		}

		criteria.setSortField(sortField);
		criteria.setAscending(ascending);

		return criteria;

	}

}
