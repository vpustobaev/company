package factory;

import entity.User;
import model.UserViewModel;

public class UserFactory {

	public User getUserFromUserViewModel(UserViewModel userViewModel) {

		Integer id = userViewModel.getId();
		String login = userViewModel.getLogin();
		String password = userViewModel.getPassword();
		String email = userViewModel.getEmail();
		String name = userViewModel.getName();
		String surname = userViewModel.getSurname();
		String address = userViewModel.getAddress();
		String position = userViewModel.getPosition();
		Integer yearsOfExperience = userViewModel.getYearsOfExperience();

		User user = new User();

		user.setId(id);
		user.setLogin(login);
		user.setPassword(password);
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setAddress(address);
		user.setPosition(position);
		user.setYearsOfExperience(yearsOfExperience);

		return user;

	}

}
