package factory;

import javax.servlet.http.HttpServletRequest;

import model.AuthorisedUserViewModel;

public class AuthorisedUserViewModelFactory {

	public AuthorisedUserViewModel getAuthorisedUserViewModel(HttpServletRequest request) {

		AuthorisedUserViewModel model = new AuthorisedUserViewModel();

		String login = request.getParameter("login").trim();
		String password = request.getParameter("password").trim();

		model.setLogin(login);
		model.setPassword(password);

		return model;
	}

}
