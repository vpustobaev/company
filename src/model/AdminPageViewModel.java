package model;

import java.util.ArrayList;

import entity.User;

public class AdminPageViewModel {

	private UserSearchCriteria criteria;
	private ArrayList<User> userList;
	private String message;

	public UserSearchCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(UserSearchCriteria criteria) {
		this.criteria = criteria;
	}

	public ArrayList<User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
