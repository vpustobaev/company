package model;

import java.util.ArrayList;

public class EditUserPageViewModel {

	private UserViewModel userViewModel;

	private ArrayList<String> inputErrors;

	public ArrayList<String> getInputErrors() {
		return inputErrors;
	}

	public void setInputErrors(ArrayList<String> inputErrors) {
		this.inputErrors = inputErrors;
	}

	public UserViewModel getUserViewModel() {
		return userViewModel;
	}

	public void setUserViewModel(UserViewModel userViewModel) {
		this.userViewModel = userViewModel;
	}

}
