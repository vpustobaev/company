package model;

public class IndexPageViewModel {

	private String message;
	private AuthorisedUserViewModel authorisedUserView;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public AuthorisedUserViewModel getAuthorisedUserView() {
		return authorisedUserView;
	}

	public void setAuthorisedUserView(AuthorisedUserViewModel authorisedUserView) {
		this.authorisedUserView = authorisedUserView;
	}

}
