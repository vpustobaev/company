package model;

public class UserSearchCriteria {

	private UserSortField sortField;
	private boolean ascending;

	public UserSortField getSortField() {
		return sortField;
	}

	public void setSortField(UserSortField sortField) {
		this.sortField = sortField;
	}

	public boolean isAscending() {
		return ascending;
	}

	public void setAscending(boolean ascending) {
		this.ascending = ascending;
	}

}
