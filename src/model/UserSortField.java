package model;

public enum UserSortField {
	
	LOGIN, EMAIL, NAME, SURNAME, ADDRESS, POSITION, YEARS;

}
