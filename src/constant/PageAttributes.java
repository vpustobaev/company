package constant;

public class PageAttributes {

	public static final String ADMIN_PAGE = "adminPage";
	public static final String USER_PAGE = "userPage";
	public static final String INDEX_PAGE = "indexPage";
	public static final String EDIT_USER_PAGE = "editUserPage";
	public static final String AUTHORISED_USER = "authorisedUSer";

}
