package constant;

public class ControllerActions {

	public static final String AUTHORISE = "authorise";
	public static final String GET_USERS = "getUsers";
	public static final String OPEN_EDITOR = "openEditor";
	public static final String UPDATE_PASS = "updatePass";
	public static final String SAVE_USER = "saveUser";
	public static final String DELETE_USER = "deleteUser";
	public static final String LOGOUT = "logout";
}
