package constant;

public class Pages {

	public static final String indexPage = "index.jsp";
	public static final String adminPage = "admin.jsp";
	public static final String userPage = "user.jsp";
	public static final String editUserPage = "editUser.jsp";

}
