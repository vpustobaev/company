package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import entity.User;
import model.UserSearchCriteria;
import util.PasswordEncryptor;

public class UserDAO extends BaseDAO<User, UserSearchCriteria> {

	public UserDAO(Connection connection) {

		super(connection);
	}

	@Override
	public ArrayList<User> getAll(UserSearchCriteria criteria) throws SQLException {

		ArrayList<User> users = new ArrayList<User>();

		String sqlQuery = "SELECT * FROM users WHERE id !=1";

		if (criteria.getSortField() != null) {

			sqlQuery += " ORDER BY " + criteria.getSortField() + " " + (criteria.isAscending() ? "ASC" : "DESC");
		}

		Statement statement = createStatement();
		ResultSet resultSet = statement.executeQuery(sqlQuery);

		while (resultSet.next()) {

			User user = new User();

			user = generateUserFromResultset(resultSet);

			users.add(user);
		}

		closeStatement(statement);

		return users;
	}

	@Override
	public void add(User user) throws SQLException {

		String login = user.getLogin();
		String password = user.getPassword();
		String email = user.getEmail();
		String name = user.getName();
		String surname = user.getSurname();
		Integer yearsOfExperience = user.getYearsOfExperience();
		String address = user.getAddress();
		String position = user.getPosition();
		Integer role = user.getRole();

		String userSqlQuery = "INSERT INTO users (login, password, email, name, surname, years, address, position, role) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement userStatement = prepareStatement(userSqlQuery);
		userStatement.setString(1, login);

		String hashedPassword = PasswordEncryptor.EncryptPassword(password);

		userStatement.setString(2, hashedPassword);
		userStatement.setString(3, email);
		userStatement.setString(4, name);
		userStatement.setString(5, surname);
		userStatement.setInt(6, yearsOfExperience);
		userStatement.setString(7, address);
		userStatement.setString(8, position);
		userStatement.setInt(9, role);
		userStatement.executeUpdate();
		closeStatement(userStatement);

	}

	@Override
	public void deleteById(Integer id) throws SQLException {

		String sqlQuery = "DELETE FROM users WHERE id = ?";

		PreparedStatement statement = prepareStatement(sqlQuery);
		statement.setInt(1, id);
		statement.executeUpdate();
		closeStatement(statement);

	}

	@Override
	public void update(User user) throws SQLException {

		String login = user.getLogin();
		String password = user.getPassword();
		String email = user.getEmail();
		String name = user.getName();
		String surname = user.getSurname();
		Integer yearsOfExperience = user.getYearsOfExperience();
		String address = user.getAddress();
		String position = user.getPosition();
		Integer id = user.getId();

		PreparedStatement userStatement = null;

		String passwordQuery = "";

		if (password != "") {

			passwordQuery = ", password=?";
		}

		String userSqlQuery = "UPDATE users SET login=?" + passwordQuery
				+ ", email=?, name=?, surname=?, years=?, address=?, position=? WHERE id = ?";

		userStatement = prepareStatement(userSqlQuery);

		if (password != "") {

			String hashedPassword = PasswordEncryptor.EncryptPassword(password);

			userStatement.setString(1, login);
			userStatement.setString(2, hashedPassword);
			userStatement.setString(3, email);
			userStatement.setString(4, name);
			userStatement.setString(5, surname);
			userStatement.setInt(6, yearsOfExperience);
			userStatement.setString(7, address);
			userStatement.setString(8, position);
			userStatement.setInt(9, id);

		} else {

			userStatement.setString(1, login);
			userStatement.setString(2, email);
			userStatement.setString(3, name);
			userStatement.setString(4, surname);
			userStatement.setInt(5, yearsOfExperience);
			userStatement.setString(6, address);
			userStatement.setString(7, position);
			userStatement.setInt(8, id);
		}

		userStatement.executeUpdate();
		closeStatement(userStatement);

	}

	public User get(String login, String password) throws SQLException {

		String sqlQuery = "SELECT * FROM users WHERE login = ?";

		PreparedStatement statement = prepareStatement(sqlQuery);
		statement.setString(1, login);

		ResultSet resultSet = statement.executeQuery();

		User user = null;

		if (resultSet.next()) {

			user = generateUserFromResultset(resultSet);

			if (!PasswordEncryptor.isPasswordValid(password, user.getPassword())) {

				user = null;
			}
		}

		closeStatement(statement);
		return user;
	}

	public boolean isUserUniqueByEmail(String email, Integer id) throws SQLException {

		boolean isUnique = true;

		String sqlQuery = "SELECT * FROM users WHERE email = ? AND id != ?";

		PreparedStatement statement = prepareStatement(sqlQuery);
		statement.setString(1, email);
		statement.setInt(2, id);
		ResultSet resultSet = statement.executeQuery();

		if (resultSet.next()) {

			isUnique = false;
		}

		closeStatement(statement);
		return isUnique;
	}

	public boolean isUserUniqueByLogin(String login, Integer id) throws SQLException {

		boolean isUnique = true;

		String sqlQuery = "SELECT * FROM users WHERE login = ? AND id != ?";

		PreparedStatement statement = prepareStatement(sqlQuery);
		statement.setString(1, login);
		statement.setInt(2, id);
		ResultSet resultSet = statement.executeQuery();

		if (resultSet.next()) {

			isUnique = false;
		}

		closeStatement(statement);
		return isUnique;
	}

	@Override
	public User getById(Integer id) throws SQLException {

		User user = null;

		String sqlQuery = "SELECT * FROM users WHERE id = ?";

		PreparedStatement statement = prepareStatement(sqlQuery);
		statement.setInt(1, id);
		ResultSet resultSet = statement.executeQuery();

		while (resultSet.next()) {

			user = generateUserFromResultset(resultSet);
		}

		closeStatement(statement);
		return user;
	}

	public User getByLogin(String login) throws SQLException {

		User user = null;

		String sqlQuery = "SELECT * FROM users WHERE login = ?";

		PreparedStatement statement = prepareStatement(sqlQuery);
		statement.setString(1, login);
		ResultSet resultSet = statement.executeQuery();

		while (resultSet.next()) {

			user = generateUserFromResultset(resultSet);
		}

		closeStatement(statement);
		return user;
	}

	public User generateUserFromResultset(ResultSet resultSet) throws SQLException {

		User user = new User();
		Integer id = resultSet.getInt(1);
		String login = resultSet.getString(2);
		String password = resultSet.getString(3);
		String email = resultSet.getString(4);
		String name = resultSet.getString(5);
		String surname = resultSet.getString(6);
		Integer yearsOfExperience = resultSet.getInt(7);
		String address = resultSet.getString(8);
		String position = resultSet.getString(9);
		Integer role = resultSet.getInt(10);

		user.setId(id);
		user.setLogin(login);

		user.setPassword(password);
		user.setEmail(email);
		user.setName(name);
		user.setSurname(surname);
		user.setYearsOfExperience(yearsOfExperience);
		user.setAddress(address);
		user.setPosition(position);
		user.setRole(role);

		return user;
	}
}
