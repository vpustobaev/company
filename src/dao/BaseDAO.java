package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import entity.Entity;

public abstract class BaseDAO<TEntity extends Entity, TSearchCriteria> extends Connector {

	protected Connection connection;

	public BaseDAO(Connection connection) {
		this.connection = connection;
	}

	public abstract List<TEntity> getAll(TSearchCriteria criteria) throws SQLException;

	public abstract TEntity getById(Integer id) throws SQLException;

	public abstract void add(TEntity entity) throws SQLException;

	public abstract void deleteById(Integer id) throws SQLException;

	public abstract void update(TEntity entity) throws SQLException;

	protected Statement createStatement() throws SQLException {

		if (connection != null) {

			Statement statement = connection.createStatement();

			if (statement != null) {

				return statement;
			}
		}

		throw new SQLException("connection or statement is null");
	}

	protected PreparedStatement prepareStatement(String sqlQuery) throws SQLException {

		if (connection != null) {

			PreparedStatement statement = connection.prepareStatement(sqlQuery);

			if (statement != null) {

				return statement;
			}
		}

		throw new SQLException("connection or statement is null");
	}

	protected void closeStatement(Statement statement) {

		if (statement != null) {

			try {
				statement.close();
			} catch (SQLException e) {

				System.err.println("statement is null " + e);
			}
		}
	}

}
