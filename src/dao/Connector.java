package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import util.ParsingUtilities;

/**
 * @author Vitaly Pustobaev
 */
public class Connector {

	private static final int URL_LINE_IN_CONFIG_FILE = 0;
	private static final int USERNAME_LINE_IN_CONFIG_FILE = 1;
	private static final int PASSWORD_LINE_IN_CONFIG_FILE = 2;
	private static final int CLASSNAME_LINE_IN_CONFIG_FILE = 3;

	public static Connection connection;

	public static Connection getConnection() {

		ArrayList<String> configData = ParsingUtilities.parseStringLinesFromFile("config.properties");

		Connection connection = null;
		try {
			Class.forName(configData.get(CLASSNAME_LINE_IN_CONFIG_FILE));
			connection = DriverManager.getConnection(configData.get(URL_LINE_IN_CONFIG_FILE),
					configData.get(USERNAME_LINE_IN_CONFIG_FILE), configData.get(PASSWORD_LINE_IN_CONFIG_FILE));
		} catch (SQLException | ClassNotFoundException e) {

			e.printStackTrace();
		}
		return connection;
	}

	public static void closeConnection(Connection con) {

		if (con != null) {

			try {
				con.close();
			} catch (SQLException e) {
				System.err.println("wrong connection " + e);
			}
		}
	}

}
