package util;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordEncryptor {

	public static String EncryptPassword(String password) {

		return BCrypt.hashpw(password, BCrypt.gensalt(12));
	}

	public static boolean isPasswordValid(String plainPassword, String hashedPassword) {

		return BCrypt.checkpw(plainPassword, hashedPassword);
	}

}
