package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ParsingUtilities {

	public static Integer tryParseInt(String string) {

		Integer result = null;

		try {

			result = Integer.valueOf(string);

		} catch (NumberFormatException e) {

			System.out.println("Wasn't able to parse the String '" + string + "' to Integer");
			result = null;
		}

		return result;
	}

	public static Date tryParseDate(String stringData, String dateFormat) {

		Date data = null;

		dateFormat = "yyyy-MM-dd";

		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		java.util.Date parsedDate = null;

		try {

			parsedDate = format.parse(stringData);

		} catch (ParseException e) {

			System.out.println("Wasn't able to parse the String '" + stringData + "' to Date");
		}

		data = new java.sql.Date(parsedDate.getTime());

		return data;

	}

	public static ArrayList<String> parseStringLinesFromFile(String path) {

		ArrayList<String> result = new ArrayList<>();

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream input = classLoader.getResourceAsStream(path);

		BufferedReader br = null;

		try {

			InputStreamReader isr = new InputStreamReader(input);

			br = new BufferedReader(isr);

			String line;

			while ((line = br.readLine()) != null) {

				result.add(line);
			}

		} catch (FileNotFoundException e) {

			System.out.println("Could not find file at the specified path.");

		} catch (IOException e) {

			System.out.println("Could not read from the File.");
		}

		return result;

	}

}
