package controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.PageAttributes;
import constant.Pages;
import model.AuthorisedUserViewModel;

/**
 * @author Vitaly Pustobaev
 *
 */
@WebFilter(urlPatterns = { "/" + Pages.adminPage, "/" + Pages.editUserPage, "/" + Pages.userPage })
public class AuthorisationFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();

		AuthorisedUserViewModel authorisedUserView = (AuthorisedUserViewModel) session
				.getAttribute(PageAttributes.AUTHORISED_USER);

		if (authorisedUserView == null) {

			response.sendRedirect(request.getContextPath() + "/" + Pages.indexPage);

		} else {

			chain.doFilter(req, res);
		}
	}

	@Override
	public void destroy() {

		Filter.super.destroy();
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		Filter.super.init(filterConfig);
	}

}
