package controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.PageAttributes;
import constant.Pages;
import model.AdminPageViewModel;
import model.UserPageViewModel;

/**
 * @author Vitaly Pustobaev
 *
 */
@WebFilter(urlPatterns = { "/" + Pages.userPage })
public class UserPageFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();

		AdminPageViewModel adminPageModel = (AdminPageViewModel) session.getAttribute(PageAttributes.ADMIN_PAGE);
		UserPageViewModel userPageModel = (UserPageViewModel) session.getAttribute(PageAttributes.USER_PAGE);

		if (adminPageModel != null && userPageModel == null) {

			response.sendRedirect(request.getContextPath() + "/" + Pages.indexPage);
		}

		else {

			chain.doFilter(req, res);
		}
	}

	@Override
	public void destroy() {

		Filter.super.destroy();
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		Filter.super.init(filterConfig);
	}

}
