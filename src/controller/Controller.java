package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.command.Command;
import controller.command.impl.CommandList;

/**
 * @author Vitaly Pustobaev
 */
@WebServlet(name = "Controller", urlPatterns = { "/Controller" })
public class Controller extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 718721383836910732L;

	private CommandList commandList;

	public Controller() {

		this.commandList = new CommandList();

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		processRequest(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		processRequest(request, response);
	}

	public void forward(HttpServletRequest request, HttpServletResponse response, String page)
			throws ServletException, IOException {

		RequestDispatcher disp = request.getRequestDispatcher(page);
		disp.forward(request, response);
	}

	public void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		Command command = this.commandList.getCommandByName(action);

		try {

			command.execute(request, response);

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

}
