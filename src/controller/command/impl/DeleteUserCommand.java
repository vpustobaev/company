package controller.command.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.ControllerActions;
import constant.Messages;
import constant.PageAttributes;
import controller.command.Command;
import dao.Connector;
import dao.UserDAO;
import model.AdminPageViewModel;
import util.ParsingUtilities;

/** @author Vitaly Pustobaev */
public class DeleteUserCommand implements Command {

	public static final String ID = "id";

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		Integer id = ParsingUtilities.tryParseInt(request.getParameter(ID));

		if (id != null) {

			Connection con = null;

			try {

				con = Connector.getConnection();

				UserDAO userDao = new UserDAO(con);
				userDao.deleteById(id);

				HttpSession session = request.getSession();

				AdminPageViewModel model = (AdminPageViewModel) session.getAttribute(PageAttributes.ADMIN_PAGE);
				model.setMessage(Messages.USER_DELETED);
				response.sendRedirect("Controller?action=" + ControllerActions.GET_USERS);

			} catch (

			SQLException e) {

				e.printStackTrace();
			}

			finally {

				Connector.closeConnection(con);
			}
		}
	}

}
