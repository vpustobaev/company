package controller.command.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controller.command.Command;
import dao.Connector;
import dao.UserDAO;
import entity.User;

/** @author Vitaly Pustobaev */
public class UpdatePasswordCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String login = request.getParameter("login").trim();
		String oldPassword = request.getParameter("oldPassword").trim();
		String newPassword = request.getParameter("newPassword").trim();

		Connection con = null;

		try {

			con = Connector.getConnection();

			UserDAO userDao = new UserDAO(con);
			User user = userDao.get(login, oldPassword);

			if (user != null) {

				user.setPassword(newPassword);
				userDao.update(user);
				response.setStatus(200);

			} else {

				response.setStatus(400);
			}

		} catch (

		SQLException e) {

			e.printStackTrace();
		}

		finally {

			Connector.closeConnection(con);
		}
	}

}
