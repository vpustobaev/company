package controller.command.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.PageAttributes;
import constant.Pages;
import controller.command.Command;
import dao.Connector;
import dao.UserDAO;
import entity.User;
import factory.UserSearchCriteriaFactory;
import model.AdminPageViewModel;
import model.UserSearchCriteria;

/** @author Vitaly Pustobaev */
public class GetUsersCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();

		UserSearchCriteria criteria = new UserSearchCriteriaFactory().getUserSearchCriteria(request);

		Connection con = null;

		try {

			con = Connector.getConnection();

			UserDAO userDao = new UserDAO(con);
			ArrayList<User> userList = userDao.getAll(criteria);

			AdminPageViewModel pageModel = (AdminPageViewModel) session.getAttribute(PageAttributes.ADMIN_PAGE);

			if (pageModel == null) {

				pageModel = new AdminPageViewModel();
			}

			pageModel.setCriteria(criteria);
			pageModel.setUserList(userList);
			session.setAttribute(PageAttributes.ADMIN_PAGE, pageModel);
			session.setAttribute(PageAttributes.USER_PAGE, null);
			session.setAttribute(PageAttributes.EDIT_USER_PAGE, null);
			response.sendRedirect(Pages.adminPage);

		} catch (SQLException e) {

			e.printStackTrace();
		}

		finally {

			Connector.closeConnection(con);
		}
	}

}
