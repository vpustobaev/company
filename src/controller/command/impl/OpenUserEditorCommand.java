package controller.command.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.PageAttributes;
import constant.Pages;
import controller.command.Command;
import dao.Connector;
import dao.UserDAO;
import entity.User;
import factory.UserViewModelFactory;
import model.EditUserPageViewModel;
import model.UserViewModel;
import util.ParsingUtilities;

/** @author Vitaly Pustobaev */
public class OpenUserEditorCommand implements Command {

	public static final String ID = "id";

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();

		Integer id = ParsingUtilities.tryParseInt(request.getParameter(ID));

		User user = null;

		if (id != null) {

			Connection con = null;

			try {

				con = Connector.getConnection();
				UserDAO userDao = new UserDAO(con);
				user = userDao.getById(id);

			} catch (

			SQLException e) {

				e.printStackTrace();
			}

			finally {

				Connector.closeConnection(con);
			}
		}

		UserViewModel userViewModel = new UserViewModelFactory().getUserViewModelFromUser(user);

		EditUserPageViewModel pageModel = new EditUserPageViewModel();
		pageModel.setUserViewModel(userViewModel);

		session.setAttribute(PageAttributes.EDIT_USER_PAGE, pageModel);
		response.sendRedirect(Pages.editUserPage);
	}

}
