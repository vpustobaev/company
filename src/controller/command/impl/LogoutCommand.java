package controller.command.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.Pages;
import controller.command.Command;

/** @author Vitaly Pustobaev */
public class LogoutCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();

		session.invalidate();

		response.sendRedirect(Pages.indexPage);

	}
}
