package controller.command.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.ControllerActions;
import constant.Messages;
import constant.PageAttributes;
import constant.Pages;
import controller.command.Command;
import dao.Connector;
import dao.UserDAO;
import entity.User;
import factory.UserFactory;
import factory.UserViewModelFactory;
import model.AdminPageViewModel;
import model.EditUserPageViewModel;
import model.UserPageViewModel;
import model.UserViewModel;
import validation.UserViewModelValidationContext;
import validation.UserViewModelValidator;

/** @author Vitaly Pustobaev */
public class SaveUserCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();

		UserViewModel userViewModel = new UserViewModelFactory().getUserViewModelFromRequestParameters(request);

		Connection con = null;

		try {

			con = Connector.getConnection();

			UserDAO dao = new UserDAO(con);

			UserViewModelValidationContext context = new UserViewModelValidationContext(userViewModel);

			validateUser(dao, context);

			User user = new UserFactory().getUserFromUserViewModel(userViewModel);

			if (context.getInputErrors().isEmpty()) {

				AdminPageViewModel adminPageModel = (AdminPageViewModel) session
						.getAttribute(PageAttributes.ADMIN_PAGE);

				if (userViewModel.getId() == 0) {

					user.setRole(2);

					dao.add(user);

					adminPageModel.setMessage(Messages.USER_CREATED);
					response.sendRedirect("Controller?action=" + ControllerActions.GET_USERS);
				}

				else {

					dao.update(user);

					if (adminPageModel != null) {

						adminPageModel.setMessage(Messages.USER_CHANGED);
						response.sendRedirect("Controller?action=" + ControllerActions.GET_USERS);

					} else {

						UserPageViewModel userPageModel = (UserPageViewModel) session
								.getAttribute(PageAttributes.USER_PAGE);
						userPageModel.setUserViewModel(userViewModel);
						userPageModel.setMessage(Messages.USER_CHANGED);
						session.setAttribute(PageAttributes.USER_PAGE, userPageModel);
						response.sendRedirect(Pages.userPage);
					}
				}

			} else {

				EditUserPageViewModel model = (EditUserPageViewModel) session
						.getAttribute(PageAttributes.EDIT_USER_PAGE);

				if (model == null) {

					model = new EditUserPageViewModel();
				}

				model.setInputErrors(context.getInputErrors());
				model.setUserViewModel(userViewModel);
				session.setAttribute(PageAttributes.EDIT_USER_PAGE, model);
				response.sendRedirect(Pages.editUserPage);
			}
		}

		catch (SQLException e) {

			e.printStackTrace();
		}

		finally {

			Connector.closeConnection(con);
		}
	}

	public void validateUser(UserDAO dao, UserViewModelValidationContext context) throws SQLException {

		UserViewModelValidator validator = new UserViewModelValidator(dao);
		validator.validate(context);
	}

}
