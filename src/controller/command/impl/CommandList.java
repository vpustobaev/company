package controller.command.impl;

import java.util.HashMap;
import java.util.Map;

import constant.ControllerActions;
import controller.command.Command;

public class CommandList {

	private Map<String, Command> commands = new HashMap<String, Command>();

	public CommandList() {

		commands.put(ControllerActions.AUTHORISE, new AuthoriseCommand());
		commands.put(ControllerActions.GET_USERS, new GetUsersCommand());
		commands.put(ControllerActions.OPEN_EDITOR, new OpenUserEditorCommand());
		commands.put(ControllerActions.UPDATE_PASS, new UpdatePasswordCommand());
		commands.put(ControllerActions.SAVE_USER, new SaveUserCommand());
		commands.put(ControllerActions.DELETE_USER, new DeleteUserCommand());
		commands.put(ControllerActions.LOGOUT, new LogoutCommand());
	}

	public Command getCommandByName(String command) {

		return getCommands().get(command);
	}

	public Map<String, Command> getCommands() {

		return commands;
	}

}
