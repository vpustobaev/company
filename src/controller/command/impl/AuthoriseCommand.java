package controller.command.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import constant.ControllerActions;
import constant.Messages;
import constant.PageAttributes;
import constant.Pages;
import controller.command.Command;
import dao.Connector;
import dao.UserDAO;
import entity.User;
import factory.AuthorisedUserViewModelFactory;
import factory.UserViewModelFactory;
import model.AuthorisedUserViewModel;
import model.IndexPageViewModel;
import model.UserPageViewModel;
import model.UserViewModel;

/** @author Vitaly Pustobaev */
public class AuthoriseCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();

		AuthorisedUserViewModel authorisedUserView = new AuthorisedUserViewModelFactory()
				.getAuthorisedUserViewModel(request);

		Connection con = null;

		try {

			con = Connector.getConnection();

			UserDAO userDao = new UserDAO(con);

			User user = userDao.get(authorisedUserView.getLogin(), authorisedUserView.getPassword());

			if (user != null) {

				session.setAttribute(PageAttributes.AUTHORISED_USER, authorisedUserView);

				if (user.getRole() == 1) {

					response.sendRedirect("Controller?action=" + ControllerActions.GET_USERS);

				} else {

					UserViewModel userViewModel = new UserViewModelFactory().getUserViewModelFromUser(user);
					UserPageViewModel pageModel = new UserPageViewModel();
					pageModel.setUserViewModel(userViewModel);
					session.setAttribute(PageAttributes.USER_PAGE, pageModel);
					session.setAttribute(PageAttributes.ADMIN_PAGE, null);
					session.setAttribute(PageAttributes.EDIT_USER_PAGE, null);
					response.sendRedirect(Pages.userPage);
				}

			} else {

				IndexPageViewModel model = new IndexPageViewModel();
				model.setMessage(Messages.AUTHORISE_ERROR);
				model.setAuthorisedUserView(authorisedUserView);
				session.setAttribute(PageAttributes.INDEX_PAGE, model);
				response.sendRedirect(Pages.indexPage);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		finally {

			Connector.closeConnection(con);
		}
	}

}
